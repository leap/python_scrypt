Source: python-scrypt
Maintainer: Micah Anderson <micah@debian.org>
Section: python
Priority: optional
Build-Depends: python-all-dev (>= 2.6.6-3), debhelper (>= 9), libssl-dev
Standards-Version: 3.9.4

Package: python-scrypt
Architecture: any
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
Description: Python bindings for the scrypt key derivation function library
 This is a set of Python bindings for the scrypt key derivation function. 
 .
 Scrypt is useful when encrypting password as it is possible to specify a
 minimum amount of time to use when encrypting and decrypting. If, for
 example, a password takes 0.05 seconds to verify, a user won't notice the
 slight delay when signing in, but doing a brute force search of several
 billion passwords will take a considerable amount of time. This is in
 contrast to more traditional hash functions such as MD5 or the SHA family
 which can be implemented extremely fast on cheap hardware.
